CPS 480 Artificially Composed Music for Music Therapy
Author: Jacob Jewell
-----------------------------------------------------

Requirements:
-------------
  - Developed in Python 3.6
  - pip install mido

To Run:
-------
  - navigate into Final Project folder
  - in command prompt: python run.py
  - to listen to song: play new_song.mid (located in Final Project/) with midi
    software
  - to read the messages of the song -> in command prompt: python read_midi.py
