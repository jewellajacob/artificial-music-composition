from mido import MidiFile

"""
This is a simple script to print out the messages of a one track midi file. When
reading the new_song.mid each note_off message will be printed out
"""

file_name = input("Enter File Name: ")

file = MidiFile(file_name)

for message in file:
    print(message)
