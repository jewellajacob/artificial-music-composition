from mido import Message, MidiFile, MidiTrack
import random

def controller():
    """
    The controller function controls the flow of the program.
    Steps:
        - call the interface that asks for song input
        - grabs the corresponding midi files to the song inputs and creates an
            array of tracks
        - finds the shortest file and its length
        - turns the array of tracks into an array of tracks where each track is
            represented by an array of midi messages, where each message is
            represented by an array of three integers, where each integer
            represents note, velocity, and time respectively
        - runs the cross over function which also calls the mutation function
        - writes one of the tracks to a midi file
    """
    song_inputs = input_interface()
    midi_files = get_midi_files(song_inputs)
    song_length = find_song_length(midi_files)
    midi_files = parse_midi_files(song_length, midi_files)
    midi_files = cross_over_function(midi_files, song_length)
    random_track = random.randint(0, 9)
    write_to_midi(midi_files[random_track])

def input_interface():
    """
    Asks the user which two songs the client responded the best to
    Gets user input, validates it, and returns it as an array
    """
    print("Welcome!")
    print("Select two musical pieces the client responded well to:")
    print(" 1) Mazurka - Borodin")
    print(" 2) Sontana No. 8 - Mozart")
    print(" 3) Pieces Book I - Grieg")
    song_one = input(" - ")
    song_one = validate_input(song_one)
    song_two = input(" - ")
    song_two = validate_input(song_two)

    return [song_one, song_two]

def validate_input(user_input):
    """
    Sticks the user in a loop until they have entered either 1, 2, or 3
    """
    while user_input != "1" and user_input != "2" and user_input != "3":
        user_input = input("Enter 1, 2, or 3: ")
    return user_input

def get_midi_files(song_inputs):
    """
    song_inputs: will be an array of the user inputs. For this program there
                    will be an array of two ints. Either 1, 2, or, 3
    Then the corresponding piano left tracks from each file is added to an array
    and returned
    """
    midi_files = []
    count = 0

    for input in song_inputs:
        if input == "1":
            midi_files.append(MidiFile("midi_dataset/Borodin/bor_ps2.mid").tracks[2])
            midi_files.append(MidiFile("midi_dataset/Borodin/bor_ps4.mid").tracks[2])
            midi_files.append(MidiFile("midi_dataset/Borodin/bor_ps5.mid").tracks[2])
            midi_files.append(MidiFile("midi_dataset/Borodin/bor_ps6.mid").tracks[2])
            midi_files.append(MidiFile("midi_dataset/Borodin/bor_ps7.mid").tracks[2])
        elif input == "2":
            midi_files.append(MidiFile("midi_dataset/Mozart/1.mid").tracks[2])
            midi_files.append(MidiFile("midi_dataset/Mozart/2.mid").tracks[2])
            midi_files.append(MidiFile("midi_dataset/Mozart/3.mid").tracks[2])
            midi_files.append(MidiFile("midi_dataset/Mozart/4.mid").tracks[2])
            midi_files.append(MidiFile("midi_dataset/Mozart/5.mid").tracks[2])
        elif input == "3":
            midi_files.append(MidiFile("midi_dataset/Grieg/grieg_album.mid").tracks[2])
            midi_files.append(MidiFile("midi_dataset/Grieg/grieg_berceuse.mid").tracks[2])
            midi_files.append(MidiFile("midi_dataset/Grieg/grieg_butterfly.mid").tracks[2])
            midi_files.append(MidiFile("midi_dataset/Grieg/grieg_march.mid").tracks[2])
            midi_files.append(MidiFile("midi_dataset/Grieg/grieg_walzer.mid").tracks[2])
    return midi_files

def find_song_length(midi_files):
    """
    Finds the shortest track, the size is determined by the number of messages
    in a track
    """
    the_length = 100000
    song_lengths = []
    current_length = 0

    #creates an array that contains the size of each track on a different index
    #only counts the note_on tracks
    for file in midi_files:
        for message in file:
            if message.type == "note_on":
                current_length += 1
        song_lengths.append(current_length)
        current_length = 0
    #compares each track size to return the smallest track
    for length in song_lengths:
        if length < the_length:
             the_length = length

    return the_length

def parse_midi_files(track_length, midi_files):
    """
    the midi_files variable is changed from an array of midi track objects to
    an array of arrays of midi messages that are represented by integers holding
    midi message values
    """
    count=0
    for track in midi_files:
        midi_files[count] = []
        message_count = 1
        for message in track:
            if message.type == "note_on" and message_count <= track_length:
                midi_files[count].append([message.note, message.velocity, message.time])
        count += 1

    return midi_files

def cross_over_function(midi_files, song_length):
    """
    this function determines if a cross over will occur and if a mutation will
    occur
    cross over rate = 90%
    mutation rate = 15%
    cross over will be attempted 1000 times
    """
    cross_over = 0
    while cross_over < 1000:
        index=0
        while index < 10:
            random_int = random.randint(1,100)
            if random_int < 90:
                midi_files[index]=do_the_crossing(index, midi_files, song_length)
            if random_int < 15:
                midi_files[index]=mutation(midi_files[index], song_length)
            index +=1
        cross_over += 1
    return midi_files

def do_the_crossing(index, midi_files, song_length):
    """
    the cross over takes the current midi track and a random one and crosses
    them over at a random index
    """
    random_int = random.randint(0,9)
    first_track = midi_files[index]
    second_track = midi_files[random_int]
    cross_over_point = random.randint(0, song_length-1)
    track = first_track[0:cross_over_point] + second_track[cross_over_point:song_length]
    return track

def mutation(track, song_length):
    """
    this function is called when mutation occurs. mutation will switch one
    random message with another in the current midi track
    """
    first_random = random.randint(0, song_length-1)
    second_random = random.randint(0, song_length-1)
    place_holder = track[first_random]
    track[first_random] = track[second_random]
    track[second_random] = place_holder
    return track

def write_to_midi(new_track):
    """
    Goes through each midi message in the midi track stored in new_track
    appends the track to the midi file and saves it. The file will be saved as
    new_song.mid in the Final Project folder
    """
    mid = MidiFile()
    track = MidiTrack()
    mid.tracks.append(track)

    track.append(Message('program_change', program=12, time=0))
    for message in new_track:
        track.append(Message('note_on', note=message[0], velocity=message[1], time=message[2]))
    mid.save('new_song.mid')

controller()
